﻿using System.Collections.Generic;

namespace Laba_2
{
    public class Paginator
    {
        public const int ITEMS_PER_PAGE = 20;
        public List<ThreatData> threadItems = new List<ThreatData>();
        public int total_items;
        public int items_remaning;
        public int last_page;

        public Paginator(int total_items, List<ThreatData> threadItems)
        {
            this.total_items = total_items;
            this.threadItems = threadItems;
            this.items_remaning = this.total_items % Paginator.ITEMS_PER_PAGE;
            this.last_page = this.total_items / Paginator.ITEMS_PER_PAGE;
        }

        public List<ThreatData> generatePage(int current_page)
        {
            List<ThreatData> threadItemsPage = new List<ThreatData>();
            int start_item = Paginator.ITEMS_PER_PAGE * current_page;

            if (current_page == this.last_page && this.items_remaning > 0)
            {
                for (int i = start_item; i < start_item + this.items_remaning; i++)
                {
                    try
                    {
                        threadItemsPage.Add(this.threadItems[i]);
                    }
                    catch
                    {
                        continue;
                    }
                }
            }
            else
            {

                for (int i = start_item; i < start_item + Paginator.ITEMS_PER_PAGE; i++)
                {
                    try
                    {
                        threadItemsPage.Add(this.threadItems[i]);
                    }
                    catch
                    {
                        continue;
                    }
                }
            }

            return threadItemsPage;

        }
    }
}
