﻿using System;

namespace Laba_2.Baseclasses
{
    public static class InitializeVariableClass
    {
        public static string url           = "https://bdu.fstec.ru/documents/files/thrlist.xlsx";
        public static string path_absolute = System.Environment.GetFolderPath(System.Environment.SpecialFolder.Personal);
        public static int curretnPage      = 0;

        //names base files
        public static readonly string file_name_xlsx     = "test.xlsx";
        public static readonly string local_storage_json = "data.json";
        public static readonly string name_save_file     = "test.json";

        //public static readonly string path_file_storage  = Android.OS.Environment.GetExternalStoragePublicDirectory(Android.OS.Environment.DirectoryDocuments).AbsolutePath;
        public static readonly string path_file_storage = "/storage/emulated/0/";
    }

}
