﻿using System;
using System.Collections.Generic;

namespace Laba_2.Baseclasses
{
    public class UpdateInfoClass
    {
        public string status;
        public List<string> error = new List<string>();
        public int count_entries;
        public List<string> initial_value = new List<string>();
        public List<string> changes = new List<string>();
    }
}
