﻿using System.Collections.Generic;
using Android.App;
using Android.Content;
using Android.Views;
using Android.Widget;
using Newtonsoft.Json;

namespace Laba_2
{
    public class ThreatAdapter : BaseAdapter<ThreatData>
    {
        List<ThreatData> items;
        Activity context;
        public ThreatAdapter(Activity context, List<ThreatData> items)
            : base()
        {
            this.context = context;
            this.items = items;
        }

        public override long GetItemId(int position)
        {
            return position;
        }

        public override ThreatData this[int position]
        {
            get { return items[position]; }
        }

        public override int Count
        {
            get { return items.Count; }
        }

        public override View GetView(int position, View convertView, ViewGroup parent)
        {
            var item = items[position];
            View view = convertView;

            if (view == null)
            {
                view = context.LayoutInflater.Inflate(Resource.Layout.list_item, null);
            }

            view.FindViewById<TextView>(Resource.Id.textView1).Text = item.Threat_identifier;
            view.FindViewById<TextView>(Resource.Id.textView2).Text = item.Threat_name;
            view.FindViewById<Button>(Resource.Id.buttonTest).Click += delegate
            {
                Intent intent = new Intent(this.context, typeof(ListItemDetailActivity));
                var data_json = JsonConvert.SerializeObject(item);
                intent.PutExtra("data", data_json);
                this.context.StartActivity(intent);
            };

            return view;
        }
    }
    
}
